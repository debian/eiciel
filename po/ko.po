# Copyright (C) 2006 SuSE Linux Products GmbH, Nuernberg
# This file is distributed under the same license as the package.
#
msgid ""
msgstr ""
"Project-Id-Version: eiciel\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-09-01 21:42+0200\n"
"PO-Revision-Date: 2018-01-29 05:04+0000\n"
"Last-Translator: Hwajin Kim <hwajin.kim@e4net.net>\n"
"Language-Team: Korean <https://l10n.opensuse.org/projects/eiciel/master/ko/"
">\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Weblate 2.18\n"

#: src/acl_editor_controller.cpp:145
msgid "Could not add ACL entry: "
msgstr "ACL 항목을 추가할 수 없음: "

#: src/acl_editor_controller.cpp:195
msgid "Could not remove ACL entry: "
msgstr "ACL 항목을 제거할 수 없음: "

#: src/acl_editor_controller.cpp:262
msgid "Could not modify ACL entry: "
msgstr "ACL 항목을 수정할 수 없음: "

#. FIXME: Can't do much here
#: src/acl_editor_controller.cpp:350
#, c-format
msgid "Exception when setting ACL of file '%s': '%s'"
msgstr ""

#. Catch-all to avoid crashing nautilus
#: src/acl_editor_controller.cpp:354
#, c-format
msgid "Unknown exception when setting ACL of file '%s'"
msgstr ""

#: src/acl_editor_controller.cpp:458 src/enclosed_acl_editor_controller.cpp:35
msgid "Are you sure you want to remove all ACL default entries?"
msgstr "ACL 기본 항목을 모두 제거하시겠습니까?"

#: src/acl_editor_widget.ui:15
#, fuzzy
msgid "Applying ACLs to enclosed files"
msgstr "<b>ACL의 현재 참가자</b>"

#: src/acl_editor_widget.ui:34
#, fuzzy
msgid "Current participants in ACL"
msgstr "<b>ACL의 현재 참가자</b>"

#: src/acl_editor_widget.ui:50 src/enclosed_acl_editor_widget.ui:76
#, fuzzy
msgid "Available participants"
msgstr "<b>가능한 참가자</b>"

#: src/acl_editor_widget.ui:65
#, fuzzy
msgid "Edit ACLs for enclosed files..."
msgstr "<b>ACL의 현재 참가자</b>"

#: src/acl_list_controller.cpp:144 src/acl_list_widget.cpp:317
msgid "Mask"
msgstr "마스크"

#: src/acl_list_controller.cpp:149
msgid "Other"
msgstr "기타"

#. reading
#. writing
#. execution
#: src/acl_list_controller.cpp:198 src/acl_list_widget.cpp:347
msgid "Default Mask"
msgstr "기본 마스크"

#. ElementKind::default_others must be the last, so we handle it here manually
#: src/acl_list_controller.cpp:205 src/acl_list_widget.cpp:342
msgid "Default Other"
msgstr "기본 기타"

#: src/acl_list_widget.cpp:100
msgid "Participant"
msgstr "참가자"

#: src/acl_list_widget.cpp:165
msgid "Read"
msgstr "읽기"

#: src/acl_list_widget.cpp:168
msgid "Write"
msgstr "쓰기"

#: src/acl_list_widget.cpp:171
msgid "Execute"
msgstr "실행"

#: src/acl_list_widget.ui:33
msgid "There are ineffective permissions"
msgstr "잘못된 권한이 있습니다"

#: src/acl_list_widget.ui:46
msgid "Edit default participants"
msgstr "기본 참가자 편집"

#: src/acl_manager.cpp:48 src/xattr_manager.cpp:34
msgid "Only regular files or directories supported"
msgstr "일반 파일이나 디렉토리만 지원됩니다"

#: src/acl_manager.cpp:465
msgid "Textual representation of the ACL is wrong"
msgstr "ACL의 문법이 잘못되었습니다"

#: src/acl_manager.cpp:486
msgid "Default textual representation of the ACL is wrong"
msgstr "ACL의 기본 문법이 잘못되었습니다"

#: src/application.cpp:44
msgid "Initial edit mode (default mode otherwise)"
msgstr ""

#: src/application.cpp:56
msgid ""
"Invalid value for 'edit-mode' option. Valid options are 'acl' or 'xattr'\n"
msgstr ""

#: src/application.cpp:96
msgid "Graphical editor of file ACLs and extended attributes"
msgstr ""

#: src/application.cpp:100
msgid "translator-credits"
msgstr ""

#: src/app_window.cpp:94 src/app_window.ui:65 src/nautilus_acl_model.cpp:181
msgid "Access Control List"
msgstr "액세스 제어 목록"

#: src/app_window.cpp:97 src/nautilus_xattr_model.cpp:46
#, fuzzy
msgid "Extended attributes"
msgstr "확장된 사용자 특성"

#: src/app_window.cpp:129
msgid "Select file"
msgstr ""

#: src/app_window.cpp:133
#, fuzzy
msgid "Select directory"
msgstr "디렉토리만"

#: src/app_window.cpp:146 src/app_window.ui:37
msgid "No file opened"
msgstr "열린 파일이 없음"

#: src/app_window.ui:16
msgid "Open file"
msgstr ""

#: src/app_window.ui:21
#, fuzzy
msgid "Open directory"
msgstr "디렉토리만"

#: src/app_window.ui:78
#, fuzzy
msgid "Extended Attributes"
msgstr "확장된 사용자 특성"

#: src/app_window.ui:99
msgid "About Eiciel"
msgstr ""

#: src/app_window.ui:103
msgid "Quit"
msgstr ""

#: src/enclosed_acl_editor_widget.ui:5
#, fuzzy
msgid "Edit ACLs for enclosed files"
msgstr "<b>ACL의 현재 참가자</b>"

#: src/enclosed_acl_editor_widget.ui:14
msgid "Cancel"
msgstr ""

#: src/enclosed_acl_editor_widget.ui:19
msgid "Apply"
msgstr ""

#: src/enclosed_acl_editor_widget.ui:44
#, fuzzy
msgid "Participants in ACL for enclosed files"
msgstr "<b>ACL의 현재 참가자</b>"

#: src/enclosed_acl_editor_widget.ui:59
#, fuzzy
msgid "Participants in ACL for enclosed directories"
msgstr "<b>ACL의 현재 참가자</b>"

#: src/nautilus_acl_model.cpp:31
msgid "Read, write and execute"
msgstr ""

#: src/nautilus_acl_model.cpp:33
msgid "Read and write"
msgstr ""

#: src/nautilus_acl_model.cpp:35
msgid "Read and execute"
msgstr ""

#: src/nautilus_acl_model.cpp:37
#, fuzzy
msgid "Only read"
msgstr "파일만"

#: src/nautilus_acl_model.cpp:39
msgid "Write and execute"
msgstr ""

#: src/nautilus_acl_model.cpp:41
#, fuzzy
msgid "Only write"
msgstr "파일만"

#: src/nautilus_acl_model.cpp:43
#, fuzzy
msgid "Only execute"
msgstr "디렉토리만"

#: src/nautilus_acl_model.cpp:45 src/nautilus_acl_model.cpp:63
#: src/nautilus_acl_model.cpp:67
msgid "No permission"
msgstr ""

#: src/nautilus_acl_model.cpp:53
msgid "List directory, access, create and remove files"
msgstr ""

#: src/nautilus_acl_model.cpp:55
msgid "List directory, no access to files"
msgstr ""

#: src/nautilus_acl_model.cpp:57
msgid "List directory and access files"
msgstr ""

#: src/nautilus_acl_model.cpp:59
msgid "List directory only, no access to files"
msgstr ""

#: src/nautilus_acl_model.cpp:61
msgid "No list directory but access, create and remove"
msgstr ""

#: src/nautilus_acl_model.cpp:65
msgid "No list directory and access files"
msgstr ""

#: src/nautilus_acl_model.cpp:101
msgid "User owner"
msgstr ""

#: src/nautilus_acl_model.cpp:109
#, fuzzy
msgid "Group owner"
msgstr "그룹"

#: src/nautilus_acl_model.cpp:115
#, c-format
msgid "User ACL: ‘%s’"
msgstr ""

#: src/nautilus_acl_model.cpp:121
#, c-format
msgid "Group ACL: ‘%s’"
msgstr ""

#: src/nautilus_acl_model.cpp:127
msgid "Mask limits Group owner, User ACL and Group ACL to"
msgstr ""

#: src/nautilus_acl_model.cpp:131
#, fuzzy
msgid "Other users"
msgstr "기타"

#: src/nautilus_acl_model.cpp:137
msgid "New files will have User owner"
msgstr ""

#: src/nautilus_acl_model.cpp:147
msgid "New files will have Group owner"
msgstr ""

#: src/nautilus_acl_model.cpp:155
#, c-format
msgid "New files will have User ACL: ‘%s’"
msgstr ""

#: src/nautilus_acl_model.cpp:163
#, c-format
msgid "New files will have Group ACL: ‘%s‘"
msgstr ""

#: src/nautilus_acl_model.cpp:170
msgid "New files will have Mask"
msgstr ""

#: src/nautilus_acl_model.cpp:175
msgid "New files will have Other users"
msgstr ""

#: src/nautilus_menu_provider.cpp:69
#, fuzzy
msgid "Edit Access Control Lists…"
msgstr "액세스 제어 목록"

#: src/nautilus_menu_provider.cpp:70 src/nautilus_menu_provider.cpp:101
#, fuzzy
msgid "Allows editing Access Control Lists"
msgstr "액세스 제어 목록"

#: src/nautilus_menu_provider.cpp:100
#, fuzzy
msgid "Edit extended attributes…"
msgstr "확장된 사용자 특성"

#: src/participant_list_widget.cpp:395
msgid "Participant not found"
msgstr "참가자를 찾을 수 없습니다"

#: src/participant_list_widget.ui:12
msgid "User"
msgstr "사용자"

#: src/participant_list_widget.ui:17
#, fuzzy
msgid "Group"
msgstr "그룹"

#: src/participant_list_widget.ui:23
msgid "Default participant"
msgstr "기본 참가자"

#: src/participant_list_widget.ui:28
msgid "Filter participants"
msgstr "참가자 필터링"

#: src/participant_list_widget.ui:51
msgid "Advanced features"
msgstr "고급 기능"

#: src/participant_list_widget.ui:61
msgid "Name of participant"
msgstr "참가자 이름"

#: src/participant_list_widget.ui:71
msgid "Search"
msgstr ""

#: src/participant_list_widget.ui:80
#, fuzzy
msgid "Show system participants"
msgstr "시스템 참가자도 표시"

#: src/xattr_editor_widget.cpp:102
msgid "Name"
msgstr "이름"

#: src/xattr_editor_widget.cpp:145
msgid "Value"
msgstr "값"

#: src/xattr_editor_widget.cpp:199
msgid "There is already an attribute with this name"
msgstr ""

#: src/xattr_editor_widget.ui:27
#, fuzzy
msgid "Name of the new attribute"
msgstr "새 특성"

#: src/xattr_editor_widget.ui:32
#, fuzzy
msgid "Value of the new attribute"
msgstr "확장된 사용자 특성"

#~ msgid "Could not rename attribute name: "
#~ msgstr "다음 특성 이름을 바꿀 수 없음: "

#~ msgid "Could not change attribute value: "
#~ msgstr "다음 특성 값을 변경할 수 없음: "

#~ msgid "Could not remove attribute: "
#~ msgstr "다음 특성을 제거할 수 없음: "

#~ msgid "New value"
#~ msgstr "새로운 값"

#~ msgid "Could not add attribute: "
#~ msgstr "다음 특성을 추가할 수 없음: "

#~ msgid "<b>File name</b>"
#~ msgstr "<b>파일 이름</b>"

#~ msgid "About..."
#~ msgstr "정보..."

#, c-format
#~ msgid "Could not show the help file: %s"
#~ msgstr "도움말 파일을 표시할 수 없습니다: %s"

#~ msgid "Choose a file or a directory"
#~ msgstr "파일 또는 디렉토리를 선택합니다"

#~ msgid "Could not open the file \""
#~ msgstr "파일을 열 수 없습니다. \""

#~ msgid "Print version information"
#~ msgstr "버전 정보 인쇄"

#~ msgid "Access control list editor"
#~ msgstr "액세스 제어 목록 편집기"

#~ msgid "Add participant to ACL"
#~ msgstr "ACL 참가자 추가"

#, fuzzy
#~ msgid "Add participant to directory ACL"
#~ msgstr "ACL 참가자 추가"

#, fuzzy
#~ msgid "Add participant to file ACL"
#~ msgstr "ACL 참가자 추가"

#~ msgid "Remove participant from ACL"
#~ msgstr "ACL에서 참가자 제거"

#~ msgid "Entry"
#~ msgstr "항목"

#~ msgid "None"
#~ msgstr "없음"

#~ msgid "Both files and directories"
#~ msgstr "파일과 디렉토리 모두"

#~ msgid "Recursion"
#~ msgstr "재귀"

#~ msgid "Default"
#~ msgstr "기본값"

#~ msgid "Default ACL"
#~ msgstr "기본 ACL"
